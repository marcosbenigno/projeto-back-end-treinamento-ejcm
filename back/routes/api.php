<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookOrderController;
use App\Http\Controllers\BookUserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderUserController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CommentUserController;
use App\Http\Controllers\CommentBookController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*Endpoint api/user*/

/*Method POST: receives Body params "name" - string, "birth" - string, "cpf" - string, "email" - string, "password" - string,"photo" - binary
returns created object*/
Route::post('/user',[UserController::class,'create']);
/*Method GET: returns all objects */
Route::get('/user',[UserController::class,'index']);
/*Method GET: receives Path param ID, returns object with ID */
Route::get('/user/{id}',[UserController::class,'show']);
/*Method DELETE: receives Path param ID, deletes object with ID; returns message*/
Route::delete('/user/{id}',[UserController::class,'destroy']);
/*Method PUT: can receive Body params "name" - string, "birth" - string, "cpf" - string, "email" - string, "password" - string, "photo" - binary; receive Path param ID
returns updatd object*/
Route::put('/user/{id}',[UserController::class,'update']);

/*Endpoint api/book*/

/*Method POST: receives Body params "id_user" - string, "name" - string, "price" - float, "condition" - string, "comment" - string, "publisher" - string, "edition" - string, "author" - string, "synopsis" - string, "cover" - binary.
returns created object*/
Route::post('/book',[BookController::class,'create']);
/*Method GET: returns all objects */
Route::get('/book',[BookController::class,'index']);
/*Method GET: receives Path param ID, returns object with ID */
Route::get('/book/{id}',[BookController::class,'show']);
/*Method DELETE: receives Path param ID, deletes object with ID; returns message*/
Route::delete('/book/{id}',[BookController::class,'destroy']);
/*Method PUT: can receive Body params "id_user" - string, "name" - string, "price" - float, "condition" - string, "comment" - string, "publisher" - string, "edition" - string, "author" - string, "synopsis" - string, "cover" - binary;
receive Path param ID
returns updated object*/
Route::put('/book/{id}',[BookController::class,'update']);

/*Endpoint api/book/order*/
/*Method PUT: receives Body param "id_order" - string;
receives Path param ID (referring to id_book)
returns updated object*/
Route::put('/book/order/{id}',[BookOrderController::class,'update']);
/*Method DELETE: receives Path param ID (referrign to id_book), deletes "id_order" from book object; returns book object*/
Route::delete('/book/order/{id}',[BookOrderController::class,'destroy']);


/*Endpoint api/book/user*/
/*Method PUT: receives Body param "id_user" - string;
receives Path param ID (referring to id_book)
returns updated object*/
Route::put('/book/user/{id}',[BookUserController::class,'update']);
/*Method DELETE: receives Path param ID (referrign to id_book), deletes "id_user" from book object; returns book object*/
Route::delete('/book/user/{id}',[BookUserController::class,'destroy']);

/*Endpoint api/order*/

/*Method POST: receives Body params "id_user" and json with book ids: "{"products: [{"id_book": ""},{"id_book": ""}]"}";
returns created object*/
Route::post('/order',[OrderController::class,'create']);
/*Method GET: returns all objects */
Route::get('/order',[OrderController::class,'index']);
/*Method GET: receives Path param ID, returns object with ID */
Route::get('/order/{id}',[OrderController::class,'show']);
/*Method DELETE: receives Path param ID, deletes object with ID; returns message*/
Route::delete('/order/{id}',[OrderController::class,'destroy']);
/*Method PUT: can receive Body params "id_user" - string, "order-date" - string, "order_end_date" - string, "products" - json;
receive Path param ID
returns updated object*/
Route::put('/order/{id}',[OrderController::class,'update']);

/*Endpoint api/order/user*/
/*Method PUT: receives Body param "id_user" - string;
receives Path param ID (referring to id_order)
returns updated object*/
Route::put('/order/user/{id}',[OrderUserController::class,'update']);
/*Method DELETE: receives Path param ID (referrign to id_order), deletes "id_user" from order object; returns order object*/
Route::delete('/order/user/{id}',[OrderUserController::class,'destroy']);



/*Endpoint api/comment*/

/*Method POST: receives Body params "id_book" - string, "id_user" - string, "rate" - integer, "comment" - string;
returns created object*/
Route::post('/comment',[CommentController::class,'create']);
/*Method GET: returns all objects */
Route::get('/comment',[CommentController::class,'index']);
/*Method GET: receives Path param ID, returns object with ID */
Route::get('/comment/{id}',[CommentController::class,'show']);
/*Method DELETE: receives Path param ID, deletes object with ID; returns message*/
Route::delete('/comment/{id}',[CommentController::class,'destroy']);
/*Method PUT: can receive Body params "id_book" - string, "id_user" - string, "rate" - integer, "comment" - string;
receive Path param ID
returns updated object*/
Route::put('/comment/{id}',[CommentController::class,'update']);

/*Endpoint api/comment/user*/
/*Method PUT: receives Body param "id_user" - string;
receives Path param ID (referring to id_comment)
returns updated object*/
Route::put('/comment/user/{id}',[CommentUserController::class,'update']);
/*Method DELETE: receives Path param ID (referrign to id_comment), deletes "id_user" from order object; returns comment object*/
Route::delete('/comment/user/{id}',[CommentUserController::class,'destroy']);


/*Endpoint api/comment/book*/
/*Method PUT: receives Body param "id_book" - string;
receives Path param ID (referring to id_comment)
returns updated object*/
Route::put('/comment/book/{id}',[CommentBookController::class,'update']);
/*Method DELETE: receives Path param ID (referrign to id_comment), deletes "id_book" from order object; returns comment object*/
Route::delete('/comment/book/{id}',[CommentBookController::class,'destroy']);






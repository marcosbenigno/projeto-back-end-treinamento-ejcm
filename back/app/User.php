<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class User extends Model
{
    public function createUser(Request $request)
    {
        $this->name = $request->name;
        $this->birth = $request->birth;
        $this->cpf = $request->cpf;
        $this->email = $request->email;
        $this->password = $request->password;
        $this->photo = $request->photo;
        $this->save();
    }

    public function updateUser(Request $request, $id)
    {

        $user = User::find($id);
        if ($request->name)
        {
            $user->name = $request->name;
        }
        if ($request->birth)
        {
            $user->birth = $request->birth;
        }
        if ($request->cpf)
        {
            $user->cpf = $request->cpf;
        }
        if ($request->email)
        {
            $user->email = $request->email;
        }
        if ($request->password)
        {
            $user->password = $request->password;
        }
        if ($request->photo)
        {
            $user->photo = $request->photo;
        }
        $user->save();
        return $user;

    }

    public function deleteUser($id)
    {

        User::destroy($id);
        if (!($this->getUser($id)))
        {
            return true;
        }
        return false;
    }

    public function getUsers()
    {
        $users = User::all();
        return $users;
    }

    public function getUser($id)
    {

        $user = User::find($id);
        return $user;

    }

    public function books()
    {
        return $this->hasMany('App\Book');
    }
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}


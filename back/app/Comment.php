<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class Comment extends Model
{
    public function createComment(Request $request)
    {

        $this->id_book = $request->id_book;
        $this->id_user = $request->id_user;
        $this->rate = $request->rate;
        $this->comment = $request->comment;

        $this->save();
        return $this;
    }

    public function updateComment(Request $request, $id)
    {
        $comment = Comment::find($id);
        if ($request->id_user)
        {
            $comment->id_user = $request->id_user;
        }
        if ($request->id_book)
        {
            $comment->id_book = $request->id_book;
        }
        if ($request->rate)
        {
            $comment->rate = $request->rate;
        }
        if ($request->comment)
        {
            $comment->comment = $request->comment;
        }
        $comment->save();
        return $comment;

    }

    public function deleteComment($id)
    {

        if (Comment::find($id))
        {
            Comment::destroy($id);
            return true;
        }
        return false;
    }

    public function getComments()
    {
        $comment = Comment::all();
        return $comment;
    }

    public function getComment($id)
    {
        if (Comment::find($id))
        {
            $comment = Comment::find($id);
            return $comment;
        }
        return false;
    }
    public function addBook(Request $request, $id)
    {
        $id_book = $request->id_book;
        $comment = Comment::findOrFail($id);
        $comment->id_book = $id_book;
        $comment->save();
        return $comment;
    }
    public function addUser(Request $request, $id)
    {
        $id_user = $request->id_user;
        $comment = Comment::findOrFail($id);
        $comment->id_user = $id_user;
        $comment->save();
        return $comment;
    }
    public function removeBook($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->id_book = null;
        $comment->save();
        return $comment;
    }
    public function removeUser($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->id_user = null;
        $comment->save();
        return $comment;
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function book()
    {
        return $this->belongsTo('App\Book');
    }

}


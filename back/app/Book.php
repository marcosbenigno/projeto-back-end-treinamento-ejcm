<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class Book extends Model
{
    public function createBook(Request $request)
    {
        $this->id_user = $request->id_user;
        $this->name = $request->name;
        $this->price = $request->price;
        $this->condition = $request->condition;
        $this->comment = $request->comment;
        $this->publisher = $request->publisher;
        $this->edition = $request->edition;
        $this->author = $request->author;
        $this->synopsis = $request->synopsis;
        $this->cover = $request->cover;

        $this->save();
    }

    public function updateBook(Request $request, $id)
    {

        $book = Book::find($id);
        if ($request->id_user)
        {
            $book->id_user = $request->id_user;
        }
        if ($request->name)
        {
            $book->name = $request->name;
        }
        if ($request->price)
        {
            $book->price = $request->price;
        }
        if ($request->condition)
        {
            $book->condition = $request->condition;
        }
        if ($request->comment)
        {
            $book->comment = $request->comment;
        }
        if ($request->publisher)
        {
            $book->publisher = $request->publisher;
        }
        if ($request->edition)
        {
            $book->edition = $request->edition;
        }
        if ($request->author)
        {
            $book->author = $request->author;
        }
        if ($request->synopsis)
        {
            $book->synopsis = $request->synopsis;
        }
        if ($request->cover)
        {
            $book->cover = $request->cover;
        }
        $book->save();
        return $book;

    }

    public function deleteBook($id)
    {
        Book::destroy($id);
        if (!($this->getBook($id)))
        {
            return true;
        }
        return false;
    }

    public function getBooks()
    {
        $books = Book::all();
        return $books;
    }

    public function getBook($id)
    {

        $book = Book::find($id);
        return $book;

    }

    public function addOrder(Request $request, $id)
    {
        $id_order = $request->id_order;
        $book = Book::findOrFail($id);
        $book->id_order = $id_order;
        $book->save();
        return $book;
    }
    public function addUser(Request $request, $id)
    {
        $id_user = $request->id_user;
        $book = Book::findOrFail($id);
        $book->id_user = $id_user;
        $book->save();
        return $book;
    }
    public function removeOrder($id)
    {

        $book = Book::findOrFail($id);
        $book->id_order = null;
        $book->save();
        return $book;
    }
    public function removeUser($id)
    {

        $book = Book::findOrFail($id);
        $book->id_user = null;
        $book->save();
        return $book;
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function comments()
    {
        return $this->hasOne('App\Comment');
    }

}


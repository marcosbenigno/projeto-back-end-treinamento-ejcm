<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookUserController extends Controller
{
    public function update(Request $request, $id)
    {
        $book = new Book;

        return response()->json(['book' => $book->addUser($request, $id) ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $book = new Book;

        return response()->json(['book' => $book->removeUser($id) ], 200);
    }
}
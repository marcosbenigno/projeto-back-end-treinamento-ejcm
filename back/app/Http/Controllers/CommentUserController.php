<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
class CommentUserController extends Controller
{
    public function update(Request $request, $id)
    {
        $comment = new Comment;

        return response()->json(['comment' => $comment->addUser($request, $id) ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $comment = new Comment;

        return response()->json(['comment' => $comment->removeUser($id) ], 200);
    }
}


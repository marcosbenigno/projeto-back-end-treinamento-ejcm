<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $user = new User;
        $user->createUser($request);
        if ($user)
        {
            return response()->json(['user' => $user], 200);
        }
    }

    public function index()
    {
        $user = new User;
        return response()->json(['users' => $user->getUsers() ], 200);
    }

    public function show($id)
    {
        $user = new User;
        if ($user->getUser($id))
        {
            return response()->json(['user' => $user->getUser($id) ], 200);
        }
        return response()->json(["Not found"], 404);
    }

    public function destroy($id)
    {
        $user = new User;
        if ($user->deleteUser($id))
        {
            return response()->json(["Success"], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $user = new User;

        return response()->json(['user' => $user->updateUser($request, $id) ], 200);
    }
}


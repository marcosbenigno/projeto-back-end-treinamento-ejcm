<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
class OrderUserController extends Controller
{
    public function update(Request $request, $id)
    {
        $order = new Order;

        return response()->json(['order' => $order->addUser($request, $id) ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $order = new Order;

        return response()->json(['order' => $order->removeUser($id) ], 200);
    }
}


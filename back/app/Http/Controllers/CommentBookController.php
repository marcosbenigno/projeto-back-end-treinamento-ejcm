<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
class CommentBookController extends Controller
{
    public function update(Request $request, $id)
    {
        $comment = new Comment;

        return response()->json(['comment' => $comment->addBook($request, $id) ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $comment = new Comment;

        return response()->json(['comment' => $comment->removeBook($id) ], 200);
    }
}


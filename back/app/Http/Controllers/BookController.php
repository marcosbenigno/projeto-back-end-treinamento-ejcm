<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
class BookController extends Controller
{
    public function create(Request $request)
    {
        $book = new Book;
        $book->createBook($request);
        if ($book)
        {
            return response()->json(['book' => $book], 200);
        }
    }

    public function index()
    {
        $book = new Book;
        return response()->json(['books' => $book->getBooks() ], 200);
    }

    public function show($id)
    {
        $book = new Book;
        if ($book->getBook($id))
        {
            return response()->json(['book' => $book->getBook($id) ], 200);
        }
        return response()->json(["Not found"], 404);
    }

    public function destroy($id)
    {
        $book = new Book;
        if ($book->deleteBook($id))
        {
            return response()->json(["Success"], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $book = new Book;

        return response()->json(['book' => $book->updateBook($request, $id) ], 200);
    }

}


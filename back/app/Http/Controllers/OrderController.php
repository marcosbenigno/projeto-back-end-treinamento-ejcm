<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
class OrderController extends Controller
{
    public function create(Request $request)
    {
        $order = new Order;
        $order->createOrder($request);
        if ($order)
        {
            return response()->json(['order' => $order], 200);
        }
    }

    public function index()
    {
        $order = new Order;
        return response()->json(['orders' => $order->getOrders() ], 200);
    }

    public function show($id)
    {
        $order = new Order;
        if ($order->getOrder($id))
        {
            return response()->json(['order' => $order->getOrder($id) ], 200);
        }
        return response()->json(["Not found"], 404);
    }

    public function destroy($id)
    {
        $order = new Order;
        if ($order->deleteOrder($id))
        {
            return response()->json(["Success"], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $order = new Order;

        return response()->json(['order' => $order->updateOrder($request, $id) ], 200);
    }

}


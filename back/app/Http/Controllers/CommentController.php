<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
class CommentController extends Controller
{
    public function create(Request $request)
    {
        $comment = new Comment;
        $comment->createComment($request);
        if ($comment)
        {
            return response()->json(['comment' => $comment], 200);
        }
    }

    public function index()
    {
        $comment = new Comment;
        return response()->json(['comment' => $comment->getComments() ], 200);
    }

    public function show($id)
    {
        $comment = new Comment;
        if ($comment->getComment($id))
        {
            return response()->json(['comment' => $comment->getComment($id) ], 200);
        }
        return response()->json(["Not found"], 404);
    }

    public function destroy($id)
    {
        $comment = new Comment;
        if ($comment->deleteComment($id))
        {
            return response()->json(["Success"], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $comment = new Comment;

        return response()->json(['comment' => $comment->updateComment($request, $id) ], 200);
    }

}


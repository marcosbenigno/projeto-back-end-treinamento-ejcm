<?php
namespace App;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function createOrder(Request $request)
    {

        $this->order_date = date("Y-m-d");
        $this->save();
        $products = $request->products;
        $jsonList = json_decode($products)->products;
        $this->setProducts($jsonList);
        $this->save();
        $this->full_price = $this->getFullPrice();

        $this->id_user = $request->id_user;

        $this->save();
    }

    //set relation on book foreign key
    public function setProducts($jsonList)
    {
        foreach ($jsonList as $product)
        {
            $idBook = $product->id_book;
            $book = Book::find($idBook);
            $book->id_order = $this->id;
            $book->save();

        }
    }

    //calculates fullprice
    public function getFullPrice()
    {
        $booksInOrder = Book::where('id_order', $this->id)
            ->get();
        $fullPrice = 0;
        foreach ($booksInOrder as $book)
        {
            $fullPrice += $book->price;
        }
        return $fullPrice;
    }

    public function updateOrder(Request $request, $id)
    {
        if ($request->id_user)
        {
            $order->id_user = $request->id_user;
        }
        if ($request->order_date)
        {
            $order->order_date = $request->order_date;
        }
        if ($request->order_end_date)
        {
            $order->order_end_date = $request->order_end_date;
        }
        if ($request->products)
        {

            $booksInOrder = Book::where('id_order', $id)->get();
            $productsToAdd = $request->products;
            //cleans foreign key on book
            foreach ($booksInOrder as $book)
            {
                $book->id_order = null;
            }
            $this->setProducts(json_decode($productsToAdd)->products);
        }
        $this->save();
        return Order::find($id);

    }

    public function deleteOrder($id)
    {

        if (Order::find($id))
        {
            Order::destroy($id);
            return true;
        }
        return false;
    }

    public function getOrders()
    {
        $orders = Order::all();
        return $orders;
    }

    public function getOrder($id)
    {
        if (Order::find($id))
        {
            $order = Order::find($id);
            return $order;
        }
        return false;
    }

    public function addUser(Request $request, $id)
    {
        $id_user = $request->id_user;
        $order = Order::findOrFail($id);
        $order->id_user = $id_user;
        $order->save();
        return $order;
    }
    public function removeUser($id)
    {
        $order = Order::findOrFail($id);
        $order->id_user = null;
        $order->save();
        return $order;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function books()
    {
        return $this->hasMany('App\Book');
    }

}


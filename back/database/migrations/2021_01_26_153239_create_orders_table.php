<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user')->nullable();
            $table->string('order_date')->nullable();
            $table->string('order_end_date')->nullable();
            $table->float('full_price')->nullable();
         
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
         
         
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

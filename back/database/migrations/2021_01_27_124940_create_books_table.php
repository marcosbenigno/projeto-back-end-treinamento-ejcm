<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->float('price')->nullable();
            $table->unsignedBigInteger('id_user')->nullable();
           
            $table->string('condition')->nullable();
            $table->string('comment')->nullable();
            $table->string('publisher')->nullable();
            $table->string('edition')->nullable();
            $table->string('author')->nullable();
            $table->string('synopsis')->nullable();
            $table->binary('cover')->nullable();
            $table->unsignedBigInteger('id_order')->nullable();
            $table->timestamps();
        });

        Schema::table('books', function (Blueprint $table) {
            
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_order')->references('id')->on('orders')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}

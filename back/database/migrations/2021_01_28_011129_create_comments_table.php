<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
        
            $table->unsignedBigInteger('id_book')->nullable();
            $table->unsignedBigInteger('id_user')->nullable();
            $table->integer('rate')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });



        Schema::table('comments', function (Blueprint $table) {
            
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('id_book')->references('id')->on('books')->onDelete('cascade');
         
         
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
